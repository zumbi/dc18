---
title: Schedule is now ready for DebConf18, in Hsinchu, Taiwan
---

We are delighted and proud to announce [the official DebConf18
schedule](/schedule/)!

Now, please do note a couple of disclaimers:

- Times and days might change since some speakers might need to
  request a reschedule due to their travel arrangements or even due to
  internal order dependencies between sessions.

- If you talk has not been selected and published, don't worry. As
  in previous years, we will have room for self-scheduled talks. We
  will detail the mechanism soon (no, really, we will!)

DebConf18 will take place in Hsinchu, Taiwan from 29 July to 5 August 2018.
It will be preceded by DebCamp, July 21 to July 27, and Open Day, July 28.

## Thank you

The DebConf18 team would like to thank all of [our sponsors](/sponsors/)
who make this event possible:

We look forward to seeing you in Hsinchu!

The DebConf team
