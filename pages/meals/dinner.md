---
name: Dinner
---
Usually, dinner is eaten at the end of the day.

Served from 18:30–20:00 daily, on the ground floor of the MIRC.
