---
name: Conference Dinner
---
Every year, we celebrate DebConf with a fancy dinner.

It will be at Jimei Seafood Restaurant 佳美海鮮餐廳 at 18:30–22:00 on
August 2.

Busses will take attendees from the venue to the dinner, at a time to be
confirmed.

There will be some brief speeches from:

1. NCTU / UST Vice Chancellor [Jason Yi-Bing Lin][en] [林一平副校長][cn]

[en]: http://www.nctu.edu.tw/vc3-member-en
[cn]: http://www.nctu.edu.tw/administration/vicepresidentoffice3/vicepresident-member

Dinner Venue Address:

* No. 715, Section 2, Yanping Road, Xiangshan District, Hsinchu City, 300
* 300新竹市香山區延平路二段715號
